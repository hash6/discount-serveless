// const server = require('server');
const server = require('./server')
const assert = require('assert').strict;
const userPb = require('../protos/protogen/js/servicev1/user_pb')
const productPb = require('../protos/protogen/js/servicev1/product_pb')

const date_to_timestamp = (timeMS) => {
  const timestamp = new proto.google.protobuf.Timestamp()
  timestamp.setSeconds(timeMS / 1000);
  timestamp.setNanos((timeMS % 1000) * 1e6);
  return timestamp
}

const create_user = (first_name, last_name, date_of_birth) => {
  const user = new userPb.User()
  user.setFirstName(first_name)
  user.setLastName(last_name)
  user.setDateOfBirth(date_to_timestamp(date_of_birth))
  return user
}

const create_product = (title, description, price_in_cents) => {
  const product = new productPb.Product()
  product.setPriceInCents(price_in_cents)
  product.setTitle(title)
  product.setDescription(description)
  return product
}

describe("apply discount teste", function() {
    var env;

    before(function () {
        env = process.env;
        const timeMS = new Date()
        process.env = { BLACKFRIDAY_DAY: timeMS.getDate(), BLACKFRIDAY_MONTH: (timeMS.getMonth()+1) };
    });

    after(function () {
        process.env = env;
    })

    it("testa a aplicação com desconto da black friday", function() {
        const timeMS = new Date()
        const response = server.applyDiscount(
            create_user("John", "Doe", timeMS),
            create_product("iphone", "celular", 100)
        )

        assert.strictEqual(90, response[0]);
        assert.strictEqual(0.1, response[1]);
    })

    it("testa a aplicação do desconto no dia do aniversário", function() {
        process.env.BLACKFRIDAY_DAY = 0;
        process.env.BLACKFRIDAY_MONTH = 0;
        const timeMS = new Date()
        const response = server.applyDiscount(
            create_user("John", "Doe", timeMS),
            create_product("iphone", "celular", 100)
        )

        assert.strictEqual(95, response[0]);
        assert.strictEqual(0.05, response[1]);
    });

    it("testa a aplicação sem desconto", function() {
        const timeMS = new Date(2018, 11, 24, 10, 33, 30, 0)
        const response = server.applyDiscount(
            create_user("John", "Doe", timeMS),
            create_product("iphone", "celular", 100)
        )

        assert.strictEqual(100, response[0]);
        assert.strictEqual(0, response[1]);
    });
})
