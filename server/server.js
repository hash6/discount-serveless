const discountGrpc = require('../protos/protogen/js/servicev1/discount_grpc_pb')
const discountPb = require('../protos/protogen/js/servicev1/discount_pb')

let applyDiscount = (user, product) => {
    const BIRTHDAY_DISCOUNT = process.env.BIRTHDAY_DISCOUNT || 0.05;
    const BLACKFRIDAY_DISCOUNT = process.env.BLACKFRIDAY_DISCOUNT || 0.10;
    const BLACKFRIDAY_DAY = process.env.BLACKFRIDAY_DAY || 25;
    const BLACKFRIDAY_MONTH = process.env.BLACKFRIDAY_MONTH || 11;

    let discount_percentage = 0.00;
    let now = new Date();
    let now_day = now.getDate()
    let now_month = now.getMonth() + 1

    let has_user_date = user.hasDateOfBirth()

    if (has_user_date) {
        let user_date_birthday = user.getDateOfBirth().toDate();
        let user_birth_day = user_date_birthday.getDate()
        let user_birth_month = user_date_birthday.getMonth() + 1
        if (user_birth_day == now_day && user_birth_month == now_month)
        {
            discount_percentage = BIRTHDAY_DISCOUNT
        }
    }

    if (now_day == BLACKFRIDAY_DAY && now_month == BLACKFRIDAY_MONTH)
    {
        discount_percentage = BLACKFRIDAY_DISCOUNT
    }

    let price_discount = product.getPriceInCents() - (product.getPriceInCents() * discount_percentage)

    return [
        parseInt(price_discount),
        discount_percentage
    ]
}


// // Method implementation of RPC discount product
let discountProduct = (call, callback) => {
    const product = call.request.getProduct()
    const user = call.request.getUser()

    // [0]: price with discount
    // [1]: discount percentage
    let discount_array = applyDiscount(user, product)
    const pbDiscount = new discountPb.Discount()
    pbDiscount.setValueInCents(discount_array[0])
    pbDiscount.setPercentage(discount_array[1])

    const discountProductResponse = new discountPb.DiscountProductResponse()
    discountProductResponse.setProduct(product)
    discountProductResponse.setDiscount(pbDiscount)

    console.log("request successfully: ", call.request.toString() )
    callback(null, discountProductResponse)
}

const start_server = () => {
  let grpc = require("grpc");

  const server = new grpc.Server();
  const SERVER_ADDRESS = process.env.PORT || "0.0.0.0:2019";
  // Add the implemented methods to the service.
  server.addService(discountGrpc.DiscountProductService, { discountProduct: discountProduct });

  server.bind(SERVER_ADDRESS, grpc.ServerCredentials.createInsecure());
  console.log("Server running.")
  server.start();
}

start_server()

module.exports = { start_server, applyDiscount } ;


