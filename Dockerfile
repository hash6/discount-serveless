FROM node:15.4

WORKDIR /src

COPY . .

RUN yarn install --production=false
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.5.0/wait /wait
RUN chmod +x /wait

EXPOSE 2019

CMD /wait && yarn start